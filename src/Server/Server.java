package Server;

import Comms.Communication;
import Comms.Listeners.SessionEventListener;
import Comms.SessionAcceptor;
import DataPool.DataPool;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * auction-system
 * Created by MAWood on 24/03/2016.
 */
public class Server implements SessionEventListener, Runnable
{
    private final HashMap<Communication, Thread> communicationThreads;
    private final HashMap<Communication, UserSession> communicationSessions;
    private final DataPool dataPool;
    private final Auctioneer auctioneer;

    public Server(int port) throws IOException
    {
        // Initialise member variables
        communicationThreads = new HashMap<>();
        communicationSessions = new HashMap<>();

        // Setup datapool
        dataPool = new DataPool();
        try
        {
            dataPool.loadDataPool();
        }
        catch (Exception e)
        {
            System.out.println("Failed to load persistence files.");
            Log.warning("Failed to load persistence files. Continuing with empty data pool.");
        }
        new Thread(dataPool).start();

        // Setup auctioneer
        auctioneer = new Auctioneer(dataPool);
        Thread auctioneerThread = new Thread(auctioneer);
        auctioneerThread.start();

        // Setup session acceptor
        SessionAcceptor sessionAcceptor = new SessionAcceptor(port);
        sessionAcceptor.addSessionEventListener(this);
        new Thread(sessionAcceptor).start();

        Log.info("Server started successfully.");

        new Thread(new ServerGUI(dataPool, this)).start();
    }

    @Override
    public void sessionStarted(Communication communication)
    {
        Log.info("New user connected.");

        Thread newThread = new Thread(communication);
        newThread.start();

        UserSession newSession = new UserSession(communication, auctioneer, dataPool);

        communicationThreads.put(communication, newThread);
        communicationSessions.put(communication, newSession);

        garbageCollection();
    }

    private void garbageCollection()
    {
        ArrayList<Communication> deadCommunications = new ArrayList<>();

        // Find all dead sessions
        communicationThreads.keySet().stream().filter(Communication::isClosed).forEach(communications -> {
            deadCommunications.add(communications);
            communicationThreads.get(communications).interrupt();
        });

        // Clean up the dead sessions
        for (Communication comm : deadCommunications)
        {
            Log.info("Cleaned up dead session" + (deadCommunications.size() > 1 ? "s":""));
            communicationThreads.remove(comm);
            communicationSessions.get(comm).close();
            communicationSessions.remove(comm);
        }
    }

    @Override
    public void run()
    {
        try
        {
            while (!Thread.interrupted())
            {
                // Clean up every 10 second
                Thread.sleep(10000);
                garbageCollection();
            }
        }
        catch (InterruptedException e)
        {
            Log.warning("Data pool thread failed to sleep.");
        }
    }

    public void close()
    {
        try
        {
            dataPool.saveDataPool();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        //private final HashMap<Communication, Thread> communicationThreads;
        //private final HashMap<Communication, UserSession> communicationSessions;

        for(Communication comm:communicationSessions.keySet())
            communicationSessions.get(comm).close();

        for(Communication comm:communicationThreads.keySet())
            communicationThreads.get(comm).interrupt();
    }
}
