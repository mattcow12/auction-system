package Server;

import java.util.ArrayList;

/**
 * auction-system - ${PACKAGE_NAME}
 * Created by matthew on 08/05/16.
 */
public class Log
{
    private static final ArrayList<LogEntry> entries = new ArrayList<>();

    public static void error(String message)
    {
        entries.add(new LogEntry(LogEntry.Level.error, message));
    }

    public static void warning(String message)
    {
        entries.add(new LogEntry(LogEntry.Level.warning, message));
    }

    public static void info(String message)
    {
        entries.add(new LogEntry(LogEntry.Level.info, message));
    }

    static void win(String message)
    {
        entries.add(new LogEntry(LogEntry.Level.win, message));
    }

    public static int getSize()
    {
        return entries.size();
    }

    public static LogEntry getEntry(int index)
    {
        return entries.get(index);
    }
}
