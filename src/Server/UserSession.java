package Server;

import Comms.Communication;
import DataPool.Bid;
import DataPool.DataPool;
import DataPool.Item;
import DataPool.User;

import java.util.Date;
import java.util.NoSuchElementException;

/**
 * auction-system
 * Created by MAWood on 27/03/2016.
 */
class UserSession implements Comms.Listeners.MessageReceivedListener
{

    private final Communication communication;
    private final Auctioneer auctioneer;
    private final DataPool dataPool;
    private int userID;
    private SessionState state;

    UserSession(Communication communication, Auctioneer auctioneer, DataPool dataPool)
    {
        this.userID = -1;
        this.communication = communication;
        this.auctioneer = auctioneer;
        this.dataPool = dataPool;
        state = SessionState.unauthenticated;
        communication.addMessageReceivedListener(this);
    }

    private void sendData(String data)
    {
        communication.sendMessage(data);
    }

    @Override
    public void messageReceived(String message)
    {
        // planned command format %command%(%data item1%,%data item2%...)
        // regex gets command in first group
        // and data items in the second
        try
        {
            UserCommand command = new UserCommand(message);

            switch (state)
            {
                case unauthenticated:
                    switch (command.getCommand())
                    {
                        case "register":
                            attemptRegister(command);
                            break;
                        case "login":
                            attemptLogin(command);
                            break;
                        default:
                            sendData("Invalid command unauthenticated user");
                            break;
                    }
                    break;
                case authenticated:
                    switch (command.getCommand())
                    {
                        case "post":
                            attemptPostAuction(command);
                            break;
                        case "bid":
                            attemptPlaceBid(command);
                            break;
                        case "request":
                            requestData(command);
                            break;
                        default:
                            sendData("Invalid command authenticated user");
                            break;
                    }
                    break;
            }
        }
        catch (Exception e)
        {
            sendData(e.getMessage());
        }
    }

    private void attemptLogin(UserCommand command)
    {
        if (command.getArgumentCount() == 2)
        {
            String username = command.getArgument(0);
            String password = command.getArgument(1);
            userID = dataPool.login(username, password);
            if (userID > -1)
            {
                state = SessionState.authenticated;
                sendData("Authenticated UserID:" + userID);
                Log.info("UserID: " + userID + " logged in.");
            } else sendData("Invalid login");
        } else
        {
            sendData("Invalid number of arguments");
        }
    }

    private void attemptRegister(UserCommand command)
    {
        if (command.getArgumentCount() == 4)
        {
            String username = command.getArgument(0);
            String name = command.getArgument(1);
            String familyName = command.getArgument(2);
            String password = command.getArgument(3);
            if (dataPool.isUsernameTaken(username))
            {
                sendData("Username taken");
                return;
            }
            dataPool.createUser(new User(username, name, familyName, password));
            userID = dataPool.login(username, password);
            state = SessionState.authenticated;
            sendData("Authenticated UserID:" + userID);
            Log.info("UserID: " + userID + " registered");
        } else
        {
            sendData("Invalid number of arguments");
        }
    }

    private void attemptPostAuction(UserCommand command)
    {
        if (command.getArgumentCount() == 6)
        {
            String title = command.getArgument(0);
            String desc = command.getArgument(1);
            String cat = command.getArgument(2);
            Date startTime = new Date(Long.parseLong(command.getArgument(3)));
            Date endTime = new Date(Long.parseLong(command.getArgument(4)));
            int reserve = Integer.parseInt(command.getArgument(5));
            auctioneer.auctionItem(new Item(title, desc, cat, userID, startTime, endTime, reserve), communication);
        } else
        {
            sendData("Invalid number of arguments");
        }
    }

    private void attemptPlaceBid(UserCommand command)
    {
        if (command.getArgumentCount() == 2)
        {
            int itemID = Integer.parseInt(command.getArgument(0));
            int price = Integer.parseInt(command.getArgument(1));
            try
            {
                System.out.println("Placing bid: " + new Bid(userID, price).toString());
                auctioneer.placeBid(new Bid(userID, price), itemID, communication);
            }
            catch (Exception e)
            {
                sendData(e.getMessage());
            }
        } else
        {
            sendData("Invalid number of arguments");
        }
    }

    private void requestData(UserCommand command)
    {
        String reply = "Invalid arguments";
        if(command.getArgumentCount() == 1)
        {
            if(command.getArgument(0).equals("items"))
            {
                reply = "items(\"";
                for(int id:dataPool.getItemIDs())
                {
                    reply+= id + "\",\"";
                }
                if(reply.length() > 8)reply = reply.substring(0, reply.length() -2);
                reply+=")";
            }
        } else if(command.getArgumentCount() == 2)
        {
            if(command.getArgument(0).equals("item"))
            {
                int itemID = Integer.parseInt(command.getArgument(1));
                if(dataPool.itemExists(itemID))
                {
                    try
                    {
                        Item item = dataPool.getItem(itemID);
                        reply = "item(\"" + item.getId() + "\",\"" +
                                item.getTitle() + "\",\"" +
                                item.getCategory() + "\",\"" +
                                item.getDescription() + "\",\"";
                                reply+= item.getHighestBid()!=null?item.getHighestBid().getPrice():0;
                                reply += "\",\"" +item.getReserve() + "\",\"" +
                                item.getStartTime().getTime() + "\",\"" +
                                item.getCloseTime().getTime() + "\",\"" +
                                item.getSellerID() + "\")";
                    }
                    catch (Exception e)
                    {
                        reply = "Item " + itemID +" does not exist:" + e.getMessage();
                    }
                } else reply = "Item does not exist";
            } else if(command.getArgument(0).equals("username"))
            {
                int userID = Integer.parseInt(command.getArgument(1));
                try
                {
                    reply = "username(\"" + dataPool.getName(userID) + "\")";
                } catch (NoSuchElementException e)
                {
                    reply = "User not found";
                }
            }

        }
        sendData(reply);
    }

    private int getUserID() throws Exception
    {
        if (userID < 0) throw new Exception("User not authenticated");
        return userID;
    }

    void close()
    {
        try
        {
            dataPool.disconnectUser(getUserID());
            Log.info("UserID: " + getUserID() + " Disconnected.");
        }
        catch (Exception e)
        {
            Log.info("Unauthenticated user Disconnected.");
        }
    }

    private enum SessionState
    {
        unauthenticated,
        authenticated
    }

}
