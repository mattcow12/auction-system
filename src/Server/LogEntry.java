package Server;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * auction-system - ${PACKAGE_NAME}
 * Created by matthew on 08/05/16.
 */
class LogEntry
{
    private final String message;
    private final Level level;
    private final Date date;

    LogEntry(Level level, String message)
    {
        this.message = message;
        this.level = level;
        date = new Date();
    }

    public Level getLevel()
    {
        return level;
    }

    public String toString()
    {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date) + " | " + level.flag() + " | " + message;
    }

    enum Level
    {
        win(" WIN "),
        error("ERROR"),
        warning("WARN "),
        info("INFO ");

        private final String flag;

        Level(String flag)
        {
            this.flag = flag;
        }

        public String flag()
        {
            return flag;
        }
    }

}
