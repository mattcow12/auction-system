package Server;

import Comms.Communication;
import DataPool.Bid;
import DataPool.DataPool;
import DataPool.Item;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * auction-system
 * Created by MAWood on 13/04/2016.
 */
class Auctioneer implements Runnable
{
    private final DataPool dataPool;
    private final ArrayList<Item> pendingItems;
    private final HashMap<Integer, ArrayList<Bid>> pendingBids;
    private final HashMap<Item, Communication> itemComms;
    private final HashMap<Bid, Communication> bidComms;


    Auctioneer(DataPool dataPool)
    {
        this.dataPool = dataPool;
        pendingItems = new ArrayList<>();
        pendingBids = new HashMap<>();
        itemComms = new HashMap<>();
        bidComms = new HashMap<>();
    }

    synchronized void placeBid(Bid bid, int itemId, Communication comms) throws Exception
    {
        if (dataPool.itemBiddable(itemId))
        {
            if (!pendingBids.containsKey(itemId)) pendingBids.put(itemId, new ArrayList<>());
            ArrayList<Bid> temp = pendingBids.get(itemId);
            temp.add(bid);
            bidComms.put(bid,comms);
            pendingBids.put(itemId, temp);
        } else
        {
            throw new Exception("Item on doesn't exist or is not up for sale");
        }
    }

    synchronized void auctionItem(Item item, Communication comms)
    {
        pendingItems.add(item);
        itemComms.put(item,comms);
    }

    @Override
    public void run()
    {
        while (!Thread.interrupted())
        {
            processNewItems();
            processNewBids();
            processItems();
            try
            {
                Thread.sleep(500);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }

    private void processNewItems()
    {
        for (Item newItem : pendingItems)
        {
            if (isValidItem(newItem))
            {
                Log.info("UserID: " + newItem.getSellerID() + " posted '" + newItem.getTitle() + "' for sale.");
                dataPool.createItem(newItem);
                itemComms.get(newItem).sendMessage("Successfully posted");
            } else
            {
                itemComms.get(newItem).sendMessage("Invalid Item");
            }
        }
        pendingItems.clear();
        itemComms.clear();
    }

    private void processNewBids()
    {
        boolean improved;
        for (int itemID : pendingBids.keySet())
        {
            improved = false;
            Bid highest = null;
            try
            {
                highest = dataPool.getItem(itemID).getHighestBid();
            }
            catch (Exception ignored)
            {
            }

            for (Bid bid : pendingBids.get(itemID))
            {
                    if (highest == null || bid.getPrice() > highest.getPrice())
                    {
                        highest = bid;
                        improved = true;
                    }
                    for (Bid loser : pendingBids.get(itemID))
                        if(!bid.equals(highest))
                            bidComms.get(loser).sendMessage("Bid failed");
            }
            try
            {
                if (improved)
                {
                    try
                    {
                        dataPool.getItem(itemID).placeBid(highest);
                        bidComms.get(highest).sendMessage("Bid successful");
                        Log.info("UserID: " + highest.getBidder() + " placed a bid of £" +
                                String.format("%.2f", ((float)highest.getPrice()/100f)) + " on itemID: " + itemID + ".");
                    }
                    catch (Exception e)
                    {
                        bidComms.get(highest).sendMessage("Bid failed");
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        pendingBids.clear();
        bidComms.clear();
    }

    private void processItems()
    {
        ArrayList<Item> unprocessed = dataPool.getUnprocessedItems();
        if (unprocessed.size() > 0)
        {
            for (Item item : unprocessed)
            {
                process(item);
                item.processed();
            }
        }
    }

    private void process(Item item)
    {
        //TODO: inform winner
        if (item.getHighestBid() != null) // if it has bids
            if (item.getHighestBid().getPrice() >= item.getReserve()) // if the reserve is met
            {
                String name = dataPool.getName(item.getHighestBid().getBidder());
                Log.win(name + " Won '" + item.getTitle() + "'. For £" +
                        String.format("%.2f", ((float)item.getHighestBid().getPrice()/100f))); // inform the winner
            }
    }

    private boolean isValidItem(Item newItem)
    {
        return newItem.getCloseTime().after(new Date()) && newItem.getCloseTime().after(newItem.getStartTime());
    }
}
