package Server;

import DataPool.DataPool;
import DataPool.User;

import javax.swing.*;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * auction-system - Server
 * Created by matthew on 09/05/16.
 */
class ServerGUI extends JFrame implements Runnable, ActionListener
{
    private static final int WIDTH = 1000;
    private static final int HEIGHT = 600;

    private final DataPool dataPool;
    private final Server server;

    private final JTextArea log;
    private final JTextArea users;
    private int count;

    private final JCheckBox winCB;
    private final JCheckBox infoCB;
    private final JCheckBox warningCB;
    private final JCheckBox errorCB;

    ServerGUI(DataPool dataPool, Server server)
    {
        super();

        this.server = server;

        // setup frame
        this.setSize(WIDTH, HEIGHT);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setTitle("Server Manager");
        this.setLayout(new BorderLayout());

        // setup log
        count = 0;
        log = new JTextArea();
        log.setEditable(false);
        DefaultCaret caret = (DefaultCaret)log.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

        // setup user list
        this.dataPool = dataPool;
        users = new JTextArea();
        users.setEditable(false);
        DefaultCaret usersCaret = (DefaultCaret)users.getCaret();
        usersCaret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

        // update data in text areas
        appendData();

        // setup scroll panes
        JScrollPane logScrollPane = new JScrollPane();
        logScrollPane.setAutoscrolls(true);
        logScrollPane.getViewport().add(log);
        logScrollPane.setPreferredSize(new Dimension(500,500));
        JScrollPane userScrollPane = new JScrollPane();
        userScrollPane.setAutoscrolls(true);
        userScrollPane.getViewport().add(users);
        userScrollPane.setPreferredSize(new Dimension(300,500));

        // setup split pane
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, logScrollPane, userScrollPane);
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(WIDTH - WIDTH/3);
        splitPane.setPreferredSize(new Dimension(WIDTH, HEIGHT));

        // add components
        this.add(splitPane, BorderLayout.CENTER);

        JPanel tray = new JPanel(new GridLayout(1,6));

        JButton saveLog = new JButton("Save log");
        tray.add(saveLog);
        saveLog.setActionCommand("save");
        saveLog.addActionListener(this);

        winCB = new JCheckBox("Wins");
        winCB.setSelected(true);
        tray.add(winCB);

        infoCB = new JCheckBox("Info");
        tray.add(infoCB);

        warningCB = new JCheckBox("Warning");
        tray.add(warningCB);

        errorCB = new JCheckBox("Error");
        tray.add(errorCB);

        JButton shutdown = new JButton("shutdown");
        shutdown.setActionCommand("shutdown");
        shutdown.addActionListener(this);
        tray.add(shutdown);

        this.add(tray,BorderLayout.SOUTH);

        this.setVisible(true);
        this.pack();
    }


    private void appendData()
    {
        int futureLogCount = Log.getSize();
        if(futureLogCount > count)
        {
            for(int i = count; i < futureLogCount; i++)
                log.setText(log.getText() + (i!=0?System.getProperty("line.separator"):"") + Log.getEntry(i).toString());
            count = futureLogCount;
        }

        User[] connectedUsers = dataPool.getConnectedUsers();
        String text = "";
        for(int i = 0; i < connectedUsers.length; i++)
            text = text + (i!=0?System.getProperty("line.separator"):"") + connectedUsers[i].getUserName();
        users.setText(text);
    }

    @Override
    public void run()
    {
        try
        {
            while (!Thread.interrupted())
            {
                Thread.sleep(1000);
                appendData();
            }
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    private void save(boolean wins, boolean info, boolean warn, boolean error) throws FileNotFoundException
    {
        PrintWriter out = new PrintWriter("auction.log");
        for(int i=0;i<Log.getSize();i++)
        {
            switch (Log.getEntry(i).getLevel())
            {
                case win:
                    if(wins) out.println(Log.getEntry(i).toString());
                    break;
                case info:
                    if(info) out.println(Log.getEntry(i).toString());
                    break;
                case warning:
                    if(warn) out.println(Log.getEntry(i).toString());
                    break;
                case error:
                    if(error) out.println(Log.getEntry(i).toString());
                    break;
            }
        }
        out.close();


    }

    @Override
    public void actionPerformed(ActionEvent actionEvent)
    {
        switch (actionEvent.getActionCommand())
        {
            case "shutdown":
                server.close();
                this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
                break;
            case "save":
                try
                {
                    save(winCB.isSelected(), infoCB.isSelected(), warningCB.isSelected(), errorCB.isSelected());
                }
                catch (FileNotFoundException e)
                {
                    Log.error("Failed to save log");
                }
                break;
        }
    }
}
