package Server;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * auction-system
 * Created by matthew on 06/05/16.
 */
public class UserCommand
{
    private final String command;
    private final ArrayList<String> arguments;

    public UserCommand(String message) throws Exception
    {
        arguments = new ArrayList<>();
        Matcher m = Pattern.compile("([a-zA-Z]+)\\((.*)\\)").matcher(message.trim());
        if (m.groupCount() < 2 || !m.matches())
        {
            throw new Exception("Invalid user command: "+ message);
        }
        command = m.group(1).toLowerCase().trim();
        if (!m.group(2).trim().equals(""))
        {
            String[] parts = m.group(2).trim().split(",");
            for (String argument : parts)
            {
                argument = argument.trim();
                if(argument.length()>1)arguments.add(argument.substring(1, argument.length() - 1));
            }
        }

    }

    public int getArgumentCount()
    {
        return arguments.size();
    }

    public String getArgument(int index)
    {
        return arguments.get(index);
    }

    public String getCommand()
    {
        return command;
    }
}
