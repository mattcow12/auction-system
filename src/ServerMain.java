import Server.Log;
import Server.Server;

import java.io.IOException;

/**
 * auction-system - PACKAGE_NAME
 * Created by matthew on 12/05/16.
 */
class ServerMain
{

    public static void main(String[] args)
    {
        try
        {
            // Start server
            new Thread(new Server(args.length < 1 ? 4444 : Integer.parseInt(args[1]))).start();
        }
        catch (IOException e)
        {
            Log.error("Failed to start server.");
            System.out.println("Failed to start server");
        }
    }
}
