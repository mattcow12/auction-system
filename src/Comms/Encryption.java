package Comms;

import javax.crypto.Cipher;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * auction-system
 * Created by MAWood on 25/03/2016.
 */
class Encryption
{
    private static final int KEY_LENGTH = 1024;
    private static final String ALGORITHM = "RSA";
    private static final String MODE = "ECB";
    private static final String PADDING = "PKCS1Padding";
    private static final String TRANSFORMATION = ALGORITHM + "/" + MODE + "/" + PADDING;
    private final KeyPair keyPair;
    private PublicKey partnersKey;

    Encryption() throws GeneralSecurityException
    {
        // generate a keypair for this session
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORITHM);
        keyPairGenerator.initialize(KEY_LENGTH);
        keyPair = keyPairGenerator.generateKeyPair();
    }

    byte[] encrypt(String message) throws Exception
    {
        X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(partnersKey.getEncoded());

        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.ENCRYPT_MODE, KeyFactory.getInstance(ALGORITHM).generatePublic(x509EncodedKeySpec));
        return cipher.doFinal(message.getBytes(StandardCharsets.UTF_8));

    }

    String decrypt(byte[] cipherText) throws Exception
    {
        PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(keyPair.getPrivate().getEncoded());

        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.DECRYPT_MODE, KeyFactory.getInstance(ALGORITHM).generatePrivate(pkcs8EncodedKeySpec));
        return new String(cipher.doFinal(cipherText));
    }

    PublicKey getPublicKey()
    {
        return keyPair.getPublic();
    }

    void setPartnersKey(PublicKey key)
    {
        this.partnersKey = key;
    }

}
