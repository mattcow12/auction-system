package Comms;

import Comms.Listeners.MessageReceivedListener;

import java.io.*;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * auction-system
 * Created by MAWood on 26/03/2016.
 */
public class Communication implements Runnable
{

    private static final String CLOSE_CODE = "*close*";
    private static final String HEARTBEAT_CODE = "*ping*";
    private static final int HEARTBEAT_FREQUENCY = 100;
    private static final int HEARTBEAT_CHANCES = 10;
    private final Socket socket;
    private final ArrayList<MessageReceivedListener> listeners;
    private DataInputStream in;
    private DataOutputStream out;
    private Encryption encryption;
    private boolean isClient = false;
    private boolean isClosed = false;

    private final ArrayList<MessageReceivedListener> removed;
    private final ArrayList<MessageReceivedListener> added;

    Communication(Socket socket)
    {
        this.socket = socket;
        listeners = new ArrayList<>();
        removed = new ArrayList<>();
        added = new ArrayList<>();
        int error = 0;
        try
        {
            // prepare public and private keys
            encryption = new Encryption();

            // prepare input and output streams
            in = new DataInputStream(socket.getInputStream()); // spawns error code 0
            error++;
            out = new DataOutputStream(socket.getOutputStream()); // spawns error code 1
            error++;

            // prepare special streams for key exchange
            ObjectOutputStream obOut = new ObjectOutputStream(socket.getOutputStream()); // spawns error code 2
            error++;
            ObjectInputStream obIn = new ObjectInputStream(socket.getInputStream()); // spawns error code 3
            error++;

            // send public key
            obOut.writeObject(encryption.getPublicKey()); // spawns error code 4
            obOut.flush();
            error++;

            // wait to receive public key from partner
            Object obj = obIn.readObject(); // spawns error code 5

            // load key into encryption system
            encryption.setPartnersKey((PublicKey) obj);
        }
        catch (IOException e)
        {
            switch (error)
            {
                case 0:
                    System.out.println("Failed to get data input stream");
                    break;
                case 1:
                    System.out.println("Failed to get data output stream");
                    break;
                case 2:
                    System.out.println("Failed to get object output stream");
                    break;
                case 3:
                    System.out.println("Failed to get object input stream");
                    break;
                case 4:
                    System.out.println("Failed to send public key");
                    break;
                case 5:
                    System.out.println("Failed to get partners public key");
                    break;
                default:
                    e.printStackTrace();
            }
        }
        catch (GeneralSecurityException e)
        {
            System.out.println("Failed to generate key pair");
        }
        catch (ClassNotFoundException e)
        {
            System.out.println("Partners key was invalid or not received.");
        }
    }

    public Communication(String hostname, int port) throws IOException
    {
        // client constructor
        this(new Socket(hostname, port));
        isClient = true;
    }

    private void sendText(String text)
    {
        try
        {
            // encrypt message for transmission
            byte[] data = encryption.encrypt(text);
            // write length of the message
            out.writeInt(data.length);
            // write the message
            out.write(data);
            // send message with length
            out.flush();
        }
        catch (Exception e)
        {
            // close the communications session if sending fails
            this.close();
        }
    }

    public void sendMessage(String text)
    {
        sendText("m" + text);
    }

    private String getText()
    {
        int length;
        try
        {
            // receive the message length
            length = in.readInt();
            if (length > 0)
            {
                byte[] message = new byte[length];
                // read the message
                in.readFully(message, 0, message.length);
                // decrypt the message
                return encryption.decrypt(message);
            }
        }
        catch (Exception e)
        {
            // close the communications session if receiving fails
            this.close();
        }
        return "";
    }

    @Override
    public void run()
    {
        // if the thread is interrupted then perform a controlled shutdown
        if (Thread.interrupted()) this.close();
        try
        {
            if (isClient)
            {
                while (!this.isClosed)
                {
                    String message = getText();
                    // if a heartbeat is received then send it back to confirm that you are alive
                    if (message.equals(HEARTBEAT_CODE))
                    {
                        if (isClient) sendText(HEARTBEAT_CODE);
                    } else
                    {
                        // remove blank messages
                        if (!message.equals(""))
                        {
                            // handle shutdown requests
                            if (message.equals(CLOSE_CODE))
                            {
                                this.close();
                            } else
                            {
                                // pass message to the application layer
                                notifyListeners(message.substring(1));
                            }
                        }
                    }
                }
            } else
            {
                int counter = 0;
                while (!this.isClosed)
                {
                    counter++;
                    // if it has been a while test the client.
                    if (counter >= HEARTBEAT_FREQUENCY)
                    {
                        counter = 0;
                        testHeartbeat();
                    } else
                    {
                        // remove blank messages
                        String message = getText();
                        if (!message.equals(""))
                        {
                            // handle shutdown requests
                            if (message.equals(CLOSE_CODE))
                            {
                                this.close();
                            } else
                            {
                                // pass message to the application layer
                                notifyListeners(message.substring(1));
                            }
                        }
                    }
                }
            }
        }
        finally
        {
            this.close();
        }
    }

    private void testHeartbeat()
    {
        // send a heartbeat message
        this.sendText(HEARTBEAT_CODE);
        String message;
        boolean passed = false;
        // check to see if a heartbeat is received soon
        for (int i = 0; i < HEARTBEAT_CHANCES; i++)
        {
            message = getText();
            if (message.equals(HEARTBEAT_CODE))
            {
                passed = true;
            } else
            {
                if (!message.equals("")) notifyListeners(message.substring(1));
            }
        }
        // perform a controlled shutdown if the heartbeat fails
        if (!passed) this.close();
    }


    private void close()
    {
        // if the close code has been send
        if (isClosed)
        {
            try
            {
                // shutdown the streams and socket
                in.close();
                out.flush();
                out.close();
                socket.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        } else
        {
            isClosed = true;
            sendText(CLOSE_CODE);
            close();
        }
    }

    private synchronized void notifyListeners(String message)
    {
        listeners.removeAll(removed);
        listeners.addAll(added);
        removed.clear();
        added.clear();
        Iterator<MessageReceivedListener> it = listeners.iterator();
        while (it.hasNext())
        {
            it.next().messageReceived(message);
        }
    }

    public synchronized void addMessageReceivedListener(MessageReceivedListener listener)
    {
        added.add(listener);
    }

    public synchronized void removeMessageReceivedListener(MessageReceivedListener listener)
    {
        removed.remove(listener);
    }

    public boolean isClosed()
    {
        return isClosed;
    }
}
