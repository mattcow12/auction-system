package Comms;

import Comms.Listeners.SessionEventListener;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

/**
 * auction-system
 * Created by MAWood on 25/03/2016.
 */
public class SessionAcceptor extends ServerSocket implements Runnable
{
    private ArrayList<SessionEventListener> sessionEventListeners;

    public SessionAcceptor(int port) throws IOException
    {
        super(port);
        sessionEventListeners = new ArrayList<>();
    }

    @Override
    public void run()
    {
        Communication newSession;
        while (!Thread.interrupted())
        {
            try
            {
                newSession = new Communication(this.accept());
                for (SessionEventListener listener : sessionEventListeners)
                    listener.sessionStarted(newSession);
            }
            catch (IOException e)
            {
                System.out.println("Failed to accept connection");
            }
        }
    }

    public void addSessionEventListener(SessionEventListener listener)
    {
        sessionEventListeners.add(listener);
    }
}
