package Comms.Listeners;

/**
 * auction-system
 * Created by MAWood on 27/03/2016.
 */
public interface MessageReceivedListener
{
    void messageReceived(String message);
}
