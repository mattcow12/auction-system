package Comms.Listeners;

import Comms.Communication;

/**
 * auction-system
 * Created by MAWood on 27/03/2016.
 */
public interface SessionEventListener
{
    void sessionStarted(Communication communication);
}
