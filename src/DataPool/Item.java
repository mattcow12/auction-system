package DataPool;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Server System:
 * Created by MAWood on 24/03/2016.
 */
public class Item implements Serializable
{

    private final String title;
    private final String description;
    private final String category;
    private final int sellerId;
    private final Date startTime;
    private final Date closeTime;
    private final int reserve;
    private final ArrayList<Bid> bids;
    private int id;
    private boolean processed;
    public Item(String title, String description, String category, int sellerId,
                Date startTime, Date closeTime, int reserve)
    {
        this.title = title;
        this.description = description;
        this.category = category;
        this.sellerId = sellerId;
        this.startTime = startTime;
        this.closeTime = closeTime;
        this.reserve = reserve;
        bids = new ArrayList<>();
        processed = false;
    }

    public Bid getHighestBid()
    {
        if (bids.size() < 1) return null;

        Bid highest = bids.get(0);

        for (Bid b : bids)
            if (b.getPrice() > highest.getPrice()) highest = b;

        return highest;

    }

    public void placeBid(Bid bid)
    {
        bids.add(bid);
    }

    public state getState()
    {
        if (!getStartTime().before(new Date())) return state.IDLE;
        if (!getCloseTime().after(new Date()))
        {
            if (processed)
            {
                return state.ENDED;
            } else
            {
                return state.PROCESSING;
            }
        }
        return state.RUNNING;
    }

    public boolean isReserveMet()
    {
        return getHighestBid().getPrice() >= reserve;
    }

    public String getTitle()
    {
        return title;
    }

    public String getDescription()
    {
        return description;
    }

    public String getCategory()
    {
        return category;
    }

    public Date getStartTime()
    {
        return startTime;
    }

    public Date getCloseTime()
    {
        return closeTime;
    }

    public int getReserve()
    {
        return reserve;
    }

    public int getSellerID()
    {
        return sellerId;
    }

    public int getId()
    {
        return id;
    }

    void setId(int id)
    {
        this.id = id;
    }

    public void processed()
    {
        processed = true;
    }

    public enum state
    {
        IDLE, RUNNING, PROCESSING, ENDED
    }
}

