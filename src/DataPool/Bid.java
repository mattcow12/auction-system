package DataPool;

import java.io.Serializable;
import java.util.Date;

/**
 * Server System:
 * Created by MAWood on 24/03/2016.
 */
public class Bid implements Serializable
{
    private final int bidder;
    private final int price;
    private final Date time;

    private Bid(int bidder, int price, Date time)
    {
        this.bidder = bidder;
        this.price = price;
        this.time = time;
    }

    public Bid(int bidder, int price)
    {
        this(bidder, price, new Date());
    }

    public int getBidder()
    {
        return bidder;
    }

    public int getPrice()
    {
        return price;
    }

    public Date getTime()
    {
        return time;
    }

    public String toString()
    {
        return bidder + " : " + String.format("%.2f", ((float)getPrice())/10);
    }
}
