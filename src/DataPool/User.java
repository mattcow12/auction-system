package DataPool;

import Server.Log;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

/**
 * Server System:
 * Created by MAWood on 24/03/2016.
 */
public class User implements Serializable
{
    private static final int iterations = 10;
    private static final int keyLength = 256;
    private static final String algorithm = "PBKDF2WithHmacSHA512";
    private final String userName;
    private final String name;
    private final String familyName;
    private final byte[] passwordHash;
    private final byte[] passwordSalt;
    private int id;

    public User(String userName, String name, String familyName, String password)
    {
        SecureRandom random = new SecureRandom();
        passwordSalt = new byte[20];
        random.nextBytes(passwordSalt);

        this.userName = userName;
        this.name = name;
        this.familyName = familyName;
        this.passwordHash = hashPassword(password);
    }

    public String getName()
    {
        return name;
    }

    public String getFamilyName()
    {
        return familyName;
    }

    public int getId()
    {
        return id;
    }

    void setId(int id)
    {
        this.id = id;
    }

    private byte[] hashPassword(String password)
    {
        // Hashing methods based off examples from the 'Open Web Application Security Project'
        // https://www.owasp.org/index.php/Hashing_Java
        try
        {
            SecretKeyFactory skf = SecretKeyFactory.getInstance(algorithm);
            PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), passwordSalt, iterations, keyLength);
            SecretKey key = skf.generateSecret(spec);
            return key.getEncoded();

        }
        catch (NoSuchAlgorithmException | InvalidKeySpecException e)
        {
            Log.error("Failed to hash user password.");
            throw new RuntimeException(e);
        }
    }

    public boolean isPasswordCorrect(String password)
    {
        return Arrays.equals(passwordHash, hashPassword(password));
    }

    public String getUserName()
    {
        return userName;
    }

    public String toString()
    {
        return id + " : " + userName + " : " + name + " " + familyName;
    }
}
