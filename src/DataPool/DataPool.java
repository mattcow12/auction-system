package DataPool;

import Server.Log;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

/**
 * auction-system
 * Created by MAWood on 27/03/2016.
 */

public class DataPool implements Runnable
{
    private static final String dataPath = "Resources/";
    private static final String itemsFileName = "items.ser";
    private static final String usersFileName = "users.ser";
    private ArrayList<User> users;
    private ArrayList<Item> items;
    private final ArrayList<User> connectedUsers;
    private boolean changed = false;

    private DataPool(ArrayList<User> users, ArrayList<Item> items)
    {
        this.users = users;
        this.items = items;
        connectedUsers = new ArrayList<>();
    }

    public DataPool()
    {
        this(new ArrayList<>(), new ArrayList<>());
    }

    public void createItem(Item item)
    {
        item.setId(this.getNextItemID());
        items.add(item);
        changed = true;
    }

    public void createUser(User user)
    {
        user.setId(this.getNextUserID());
        users.add(user);
        changed = true;
    }

    public int login(String userName, String password)
    {
        for (User u : users)
        {
            if (u.getUserName().equals(userName))
            {
                if(u.isPasswordCorrect(password))
                {
                    connectedUsers.add(u);
                    return u.getId();
                }
            }
        }
        return -1;
    }

    public boolean itemExists(int targetItem)
    {
        for (Item item : items)
        {
            if (item.getId() == targetItem) return true;
        }
        return false;
    }

    public Item getItem(int id) throws Exception
    {
        changed = true;
        for (Item item : items)
        {
            if (item.getId() == id) return item;
        }
        throw new Exception("Item not found");
    }

    public ArrayList<Integer> getItemIDs()
    {
        return items.stream().map(item -> item.getId()).collect(Collectors.toCollection(ArrayList::new));
    }


    public boolean itemBiddable(int targetItem)
    {
        for (Item item : items)
        {
            if (item.getId() == targetItem) return item.getState() == Item.state.RUNNING;
        }
        return false;
    }

    public ArrayList<Item> getUnprocessedItems()
    {
        return items.stream().filter(item -> item.getState() == Item.state.PROCESSING).collect(Collectors.toCollection(ArrayList::new));
    }

    private int getNextUserID() throws NoSuchElementException
    {
        // populate array with all user ids (arrays are faster then iterating over arraylist of user)
        int id = 0;
        int[] ids = new int[users.size()];
        for (User u : users)
        {
            ids[id] = u.getId();
            id++;
        }
        return getNextID(ids);
    }

    private int getNextItemID() throws NoSuchElementException
    {
        // populate array with all user ids (arrays are faster then iterating over arraylist of user)
        int id = 0;
        int[] ids = new int[items.size()];
        for (Item u : items)
        {
            ids[id] = u.getId();
            id++;
        }
        return getNextID(ids);
    }

    private int getNextID(int[] existingIDs) throws NoSuchElementException
    {
        // check all possible IDs (integer)
        for (int possibleID = 0; possibleID < Integer.MAX_VALUE; possibleID++)
        {
            // check if the ID is taken
            boolean contains = false;
            for (int existingID : existingIDs)
                if (existingID == possibleID)
                {
                    contains = true;
                    break;
                }
            if (contains) continue;
            return possibleID;
        }
        throw new NoSuchElementException();
    }

    public boolean isUsernameTaken(String username)
    {
        for (User user : users) if (user.getUserName().equals(username)) return true;
        return false;
    }


    @SuppressWarnings("unchecked")
    public void loadDataPool() throws Exception
    {
        try
        {
            FileInputStream fileIn = new FileInputStream(dataPath + usersFileName);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            this.users = (ArrayList<User>) in.readObject();
            in.close();
            fileIn.close();

            fileIn = new FileInputStream(dataPath + itemsFileName);
            in = new ObjectInputStream(fileIn);
            this.items = (ArrayList<Item>) in.readObject();
            in.close();
            fileIn.close();
            changed = true;

        }
        catch (ClassNotFoundException e)
        {
            Log.error("Persistence files corrupted or invalid.");
        }
    }


    public void saveDataPool() throws IOException
    {
        //noinspection ResultOfMethodCallIgnored
        new File(dataPath).mkdirs();
        Log.info("Saving datapool.");
        FileOutputStream fileOut = new FileOutputStream(dataPath + usersFileName, false);
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(users);
        out.close();
        fileOut.close();

        fileOut = new FileOutputStream(dataPath + itemsFileName, false);
        out = new ObjectOutputStream(fileOut);
        out.writeObject(items);
        out.close();
        fileOut.close();
        changed = false;
    }

    @Override
    public void run()
    {
        try
        {
            while (!Thread.interrupted())
            {
                Thread.sleep(1000);
                if (changed) saveDataPool();
            }
        }
        catch (InterruptedException | IOException e)
        {
            Log.warning("Data pool thread failed to sleep.");
        }
    }

    public void disconnectUser(int userID)
    {
        Iterator<User> users = connectedUsers.iterator();
        while(users.hasNext())
        {
            User user = users.next();
            if(user.getId() == userID) users.remove();
        }
    }

    public User[] getConnectedUsers()
    {
        return connectedUsers.toArray(new User[connectedUsers.size()]);
    }

    public String getName(int userID) throws NoSuchElementException
    {
        for(User user:users)
        {
            if(user.getId() == userID) return user.getName() + " " + user.getFamilyName();
        }
        throw new NoSuchElementException();
    }
}
