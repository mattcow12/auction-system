package Client;

import Comms.Communication;
import Comms.Listeners.MessageReceivedListener;

import DataPool.Item;
import Server.UserCommand;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.NoSuchElementException;

/**
 * auction-system - ${PACKAGE_NAME}
 * Created by matthew on 08/05/16.
 */
class ClientDataManager implements Runnable, MessageReceivedListener
{
    enum field
    {
        ITEM_ID(0),
        ITEM_TITLE(1),
        ITEM_CATEGORY(2),
        ITEM_DESCRIPTION(3),
        ITEM_PRICE(4),
        ITEM_RESERVE(5),
        ITEM_START(6),
        ITEM_END(7),
        ITEM_SELLER(8);

        private final int id;
        field(int id) { this.id = id; }
        public int getValue() { return id; }
    }
    enum simplifiedField
    {
        ITEM_ID(0),
        ITEM_TITLE(1),
        ITEM_PRICE(2),
        ITEM_END(3),
        ITEM_SELLER(4);

        private final int id;
        simplifiedField(int id) { this.id = id; }
        public int getValue() { return id; }
    }

    private final Communication communication;

    private HashSet<Integer> itemIDs;

    private ArrayList<String[]> data;
    private ArrayList<String[]> filteredData;

    ClientDataManager(Communication communication)
    {
        data = new ArrayList<>();
        filteredData = new ArrayList<>();
        this.communication = communication;
        itemIDs = new HashSet<>();
        communication.addMessageReceivedListener(this);
        gatherData();
    }

    void gatherData()
    {
        for(Integer id:itemIDs)
        {
            communication.sendMessage("request(\"item\",\"" + id + "\")");
        }
    }

    String[] getItem(int id) throws NoSuchElementException
    {
        for(String[] item:data) if(Integer.parseInt(item[field.ITEM_ID.getValue()]) == id) return item;
        throw new NoSuchElementException();
    }

    private void filterData(field fld, String operator, String operand)
    {
        filteredData.clear();
        for(String[] item:data)
        {
            switch (operator)
            {
                case "equals":
                    if(item[fld.getValue()].equals(operand)) filteredData.add(item);
                    break;
                case "contains":
                    if(item[fld.getValue()].contains(operand)) filteredData.add(item);
                    break;
                default:
                    filteredData.add(item);
            }
        }
    }

    String[][] simplifyData(ArrayList<String[]> data)
    {
        String[][] simplified = new String[data.size()][5];
        for(int i=0; i<data.size(); i++)
        {
            simplified[i][simplifiedField.ITEM_ID.getValue()] = data.get(i)[field.ITEM_ID.getValue()];
            simplified[i][simplifiedField.ITEM_TITLE.getValue()] = data.get(i)[field.ITEM_TITLE.getValue()];
            simplified[i][simplifiedField.ITEM_PRICE.getValue()] = data.get(i)[field.ITEM_PRICE.getValue()];
            simplified[i][simplifiedField.ITEM_END.getValue()] = data.get(i)[field.ITEM_END.getValue()];
            simplified[i][simplifiedField.ITEM_SELLER.getValue()] = data.get(i)[field.ITEM_SELLER.getValue()];
        }
        return simplified;
    }

    ArrayList<String[]> getData()
    {
        return data;
    }

    ArrayList<String[]> getFilteredData(field fld, String operator, String operand)
    {
        filterData(fld, operator, operand);
        return filteredData;
    }

    @Override
    public void run()
    {
        try
        {
            communication.sendMessage("request(\"items\")");
            Thread.sleep(10000);
            gatherData();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void messageReceived(String message)
    {
        try
        {
            UserCommand command = new UserCommand(message);
            if(command.getCommand().equals("items"))
            {
                itemIDs.clear();
                for(int i=0; i<command.getArgumentCount();i++) itemIDs.add(Integer.parseInt(command.getArgument(i)));
                gatherData();
            } else if(command.getCommand().equals("item"))
            {
                String[] newItem = new String[9];
                newItem[field.ITEM_ID.getValue()] = command.getArgument(0);
                newItem[field.ITEM_TITLE.getValue()] = command.getArgument(1);
                newItem[field.ITEM_CATEGORY.getValue()] = command.getArgument(2);
                newItem[field.ITEM_DESCRIPTION.getValue()] = command.getArgument(3);
                newItem[field.ITEM_PRICE.getValue()] = "£" +
                        String.format("%.2f", ((float)Integer.parseInt(command.getArgument(4))/100f));
                newItem[field.ITEM_RESERVE.getValue()] = command.getArgument(5);
                newItem[field.ITEM_START.getValue()] =
                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(
                                new Date(Long.parseLong(command.getArgument(6))));
                newItem[field.ITEM_END.getValue()] =
                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(
                                new Date(Long.parseLong(command.getArgument(7))));
                newItem[field.ITEM_SELLER.getValue()] = command.getArgument(8);
                int target = -1;
                for(String[] item:data)
                    if(item[field.ITEM_ID.getValue()].equals(newItem[field.ITEM_ID.getValue()]))
                        target = data.indexOf(item);
                if(target >= 0)
                {
                    data.set(target, newItem);
                } else
                {
                    data.add(newItem);
                }

            }

        }
        catch (Exception ignored)
        {
        }
    }
}
