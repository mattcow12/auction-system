package Client;

import Comms.Communication;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * auction-system - ${PACKAGE_NAME}
 * Created by matthew on 08/05/16.
 */
class AuctionFrame extends JFrame
{
    private final ClientDataManager dataManager;

    private final DefaultTableModel model;
    private final JTable table;
    private final JComboBox<ClientDataManager.field> fieldSelect;
    private final JComboBox<String> operation;
    private final JTextField filter;

    AuctionFrame(ClientDataManager dataManager, Communication communication)
    {
        this.dataManager = dataManager;
        model = new DefaultTableModel()
        {
            @Override
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        model.addColumn("ItemID");
        model.addColumn("Title");
        model.addColumn("Price");
        model.addColumn("End Date");
        model.addColumn("Seller");
        table = new JTable(model);

        table.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                Point p = me.getPoint();
                int row = table.rowAtPoint(p);
                if (me.getClickCount() == 2) {
                    new ItemViewFrame(dataManager, Integer.parseInt(String.valueOf(model.getValueAt(row,0))));
                }
            }
        });

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(500, 500);
        this.setLayout(new BorderLayout());
        this.add(new JScrollPane(table), BorderLayout.CENTER);
        JPanel bottomPanel = new JPanel(new BorderLayout());
        JPanel buttonPanel = new JPanel(new GridLayout(1,3));
        JPanel filterPanel = new JPanel(new GridLayout(1,3));
        bottomPanel.add(filterPanel, BorderLayout.CENTER);
        fieldSelect = new JComboBox<>();
        fieldSelect.setModel(new DefaultComboBoxModel<>(ClientDataManager.field.values()));
        fieldSelect.setSelectedIndex(0);
        filterPanel.add(fieldSelect);
        operation = new JComboBox<>(new String[] {"none", "contains", "equals" });
        operation.setSelectedIndex(0);
        filterPanel.add(operation);
        filter = new JTextField();
        filter.setText("");
        filterPanel.add(filter);
        bottomPanel.add(buttonPanel, BorderLayout.SOUTH);
        JButton refresh = new JButton("Refresh");
        refresh.addActionListener(actionEvent -> {
            try {
                dataManager.gatherData();
                Thread.sleep(1000);
                updateData();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        JButton bid = new JButton("Bid");
        bid.addActionListener(actionEvent -> {
            new BidFrame(communication, Integer.parseInt(model.getValueAt(table.getSelectedRow(),0).toString()));
        });
        JButton postItem = new JButton("Post Item");
        postItem.addActionListener(actionEvent -> {
            new ItemFrame(communication);
        });
        buttonPanel.add(refresh);
        buttonPanel.add(bid);
        buttonPanel.add(postItem);
        this.add(bottomPanel, BorderLayout.SOUTH);
        updateData();
        this.setVisible(true);
    }

    private void updateData()
    {
        model.setNumRows(0);
        ArrayList<String[]> items= dataManager.getFilteredData((ClientDataManager.field) fieldSelect.getSelectedItem(),
                        (String)operation.getSelectedItem(),
                        filter.getText());
        for(String[] data:dataManager.simplifyData(items)) model.addRow(data);
    }
}
