package Client;

import Comms.Communication;
import Comms.Listeners.MessageReceivedListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;

/**
 * auction-system - ${PACKAGE_NAME}
 * Created by matthew on 08/05/16.
 */
class LoginFrame extends JFrame implements MessageReceivedListener
{
    private final Client client;
    private final Communication communication;
    private static boolean open;

    private final JButton loginButton;
    private final JTextField usernameField;
    private final JPasswordField passwordField;


    LoginFrame(Communication communication, Client client) throws Exception
    {
        this.communication = communication;
        if(open) throw new Exception("Already open");
        open = true;
        this.client = client;
        communication.addMessageReceivedListener(this);
        this.setSize(300,100);
        this.setTitle("Login");
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setLayout(new GridLayout(3,1));

        JPanel usernamePanel = new JPanel();
        usernamePanel.setLayout(new BorderLayout());
        usernamePanel.add(new JLabel("Username:"), BorderLayout.WEST);
        usernameField = new JTextField();
        usernamePanel.add(usernameField, BorderLayout.CENTER);

        JPanel passwordPanel = new JPanel();
        passwordPanel.setLayout(new BorderLayout());
        passwordPanel.add(new JLabel("Password: "), BorderLayout.WEST);
        passwordField = new JPasswordField();
        passwordPanel.add(passwordField, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel(new GridLayout(1,2));

        loginButton = new JButton("Login");
        this.getRootPane().setDefaultButton(loginButton);
        loginButton.addActionListener(actionEvent -> {
            loginButton.setEnabled(false);
            communication.sendMessage("login(\"" + usernameField.getText().trim() + "\",\"" + String.valueOf(passwordField.getPassword()) + "\")");
        });

        JButton registerButton = new JButton("Register");

        registerButton.addActionListener(actionEvent -> {
            new RegisterFrame(communication,client);
            this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        });

        buttonPanel.add(registerButton);
        buttonPanel.add(loginButton);

        this.add(usernamePanel);
        this.add(passwordPanel);
        this.add(buttonPanel);

        this.setVisible(true);
    }

    @Override
    public void messageReceived(String message)
    {
        if(message.contains("Authenticated"))
        {
            communication.removeMessageReceivedListener(this);
            client.authenticated();
            this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        }
        else
        {
            loginButton.setEnabled(true);
        }
    }
}
