package Client;

import Comms.Communication;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;

/**
 * auction-system - Client
 * Created by matthew on 13/05/16.
 */
class ItemFrame extends JFrame
{

    ItemFrame(Communication communication)
    {
        this.setLayout(new GridLayout(7,1));
        this.setSize(200,600);
        this.setTitle("Bid");
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        JTextField titleT = new JTextField();
        JTextField descT = new JTextField();
        JTextField catT = new JTextField();
        JTextField startT = new JTextField();
        JTextField endT = new JTextField();
        JTextField reserveT = new JTextField();

        this.add(createField("Title", titleT));
        this.add(createField("Description", descT));
        this.add(createField("Category", catT));
        this.add(createField("Start Time (epoch milli)", startT));
        this.add(createField("End Time (epoch milli)", endT));
        this.add(createField("Reserve", reserveT));

        JButton postItem = new JButton("Post item");

        postItem.addActionListener(actionEvent -> {
            String title = titleT.getText();
            String desc = descT.getText();
            String cat = catT.getText();

            long start = Long.parseLong(startT.getText());
            long end = Long.parseLong(endT.getText());

            int reserve = Math.round(Float.parseFloat(reserveT.getText())*100);

            communication.sendMessage("post(\"" + title +
                    "\",\"" + desc +
                    "\",\"" + cat +
                    "\",\"" + start +
                    "\",\"" + end +
                    "\",\"" + reserve +
                    "\")");
            this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        });
        this.add(postItem);
        this.setVisible(true);
    }

    JPanel createField(String label, JTextField field)
    {
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(new Label(label), BorderLayout.NORTH);
        panel.add(field, BorderLayout.CENTER);
        return panel;
    }

    void close()
    {
        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }
}
