package Client;

import Comms.Communication;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

/**
 * auction-system - Client
 * Created by matthew on 13/05/16.
 */
class BidFrame extends JFrame
{
    BidFrame(Communication communication, int itemID)
    {
        this.setLayout(new GridLayout(2,1));
        this.setSize(200,100);
        this.setTitle("Bid");
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        JTextField bidAmount = new JTextField();
        this.add(bidAmount);
        JButton placeBid = new JButton("PlaceBid");
        placeBid.addActionListener(actionEvent -> {
            double price = Double.parseDouble(bidAmount.getText());
            if(price > 0.0d)
            {
                communication.sendMessage("bid(\"" + itemID + "\",\"" + Math.round(price*100) + "\")");
                this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
            }
        });
        this.add(placeBid);
        this.setVisible(true);
    }
}
