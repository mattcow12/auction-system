package Client;

import Comms.Communication;
import Comms.Listeners.MessageReceivedListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;

/**
 * auction-system - ${PACKAGE_NAME}
 * Created by matthew on 08/05/16.
 */
class RegisterFrame extends JFrame implements MessageReceivedListener
{
    private final Client client;
    private final Communication communication;

    private final JTextField usernameField;
    private final JTextField nameField;
    private final JTextField familyNameField;
    private final JPasswordField passwordField;

    private final JButton registerButton;

    RegisterFrame(Communication communication, Client client)
    {
        this.client = client;
        this.communication = communication;
        communication.addMessageReceivedListener(this);
        this.setSize(400,200);
        this.setTitle("Register");
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setLayout(new GridLayout(5,1));

        JPanel usernamePanel = new JPanel(new BorderLayout());
        usernamePanel.add(new JLabel("Username:    "), BorderLayout.WEST);
        usernameField = new JTextField();
        usernamePanel.add(usernameField, BorderLayout.CENTER);

        JPanel namePanel = new JPanel(new BorderLayout());
        namePanel.add(new JLabel("Name:            "), BorderLayout.WEST);
        nameField = new JTextField();
        namePanel.add(nameField, BorderLayout.CENTER);

        JPanel familyNamePanel = new JPanel(new BorderLayout());
        familyNamePanel.add(new JLabel("Family Name:"), BorderLayout.WEST);
        familyNameField = new JTextField();
        familyNamePanel.add(familyNameField, BorderLayout.CENTER);

        JPanel passwordPanel = new JPanel(new BorderLayout());
        passwordPanel.add(new JLabel("Password:     "), BorderLayout.WEST);
        passwordField = new JPasswordField();
        passwordPanel.add(passwordField, BorderLayout.CENTER);

        registerButton = new JButton("Register");
        registerButton.addActionListener(actionEvent ->
        {
            registerButton.setEnabled(false);
            communication.sendMessage("register(\"" +
                    usernameField.getText().trim() + "\",\"" +
                    nameField.getText().trim() + "\",\"" +
                    familyNameField.getText().trim() + "\",\"" +
                    String.valueOf(passwordField.getPassword()) + "\")");
        });

        this.add(usernamePanel);
        this.add(namePanel);
        this.add(familyNamePanel);
        this.add(passwordPanel);
        this.add(registerButton);

        this.setVisible(true);
    }

    @Override
    public void messageReceived(String message)
    {
        if(message.contains("Authenticated"))
        {
            communication.removeMessageReceivedListener(this);
            client.authenticated();
            this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        }
        else
        {
            registerButton.setEnabled(true);
        }
    }
}
