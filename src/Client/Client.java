package Client;

import Comms.Communication;
import Comms.Listeners.MessageReceivedListener;

import java.awt.*;
import java.io.IOException;
import java.util.Scanner;

/**
 * auction-system
 * Created by MAWood on 24/03/2016.
 */
public class Client
{
    private boolean authenticated = false;

    public Client(String host, int port)
    {
        Thread clientThread;
        Communication communication;
        try
        {
            communication = new Communication(host, port);
            clientThread = new Thread(communication);
            clientThread.start();

            LoginFrame lf;
            while(!authenticated)
            {
                try
                {
                    lf = new LoginFrame(communication, this);
                    Thread.sleep(1000);
                }
                catch (Exception e)
                {
                }
            }
            System.out.println("Authenticated");
            ClientDataManager dataManager = new ClientDataManager(communication);
            new Thread(dataManager).start();
            try
            {
                Thread.sleep(2000);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            AuctionFrame auctionFrame = new AuctionFrame(dataManager,communication);
        }
        catch (IOException e)
        {
            System.out.println("Failed to connect to the server");
        }
    }

    void authenticated()
    {
        authenticated = true;
    }
}
