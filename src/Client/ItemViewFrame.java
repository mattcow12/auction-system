package Client;

import javax.swing.*;
import java.awt.*;
import java.util.NoSuchElementException;

/**
 * auction-system - Client
 * Created by matthew on 13/05/16.
 */
class ItemViewFrame extends JFrame
{
    ItemViewFrame(ClientDataManager clientDataManager, int itemID)
    {
        try
        {
            this.setSize(300,300);
            this.setTitle("Detailed view");
            this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            String[] data = clientDataManager.getItem(itemID);
            this.setLayout(new GridLayout(8,1));
            this.add(new JLabel("ID: " + data[ClientDataManager.field.ITEM_ID.getValue()]));
            this.add(new JLabel("Title: " + data[ClientDataManager.field.ITEM_TITLE.getValue()]));
            this.add(new JLabel("Description: " + data[ClientDataManager.field.ITEM_DESCRIPTION.getValue()]));
            this.add(new JLabel("Category: " + data[ClientDataManager.field.ITEM_CATEGORY.getValue()]));
            this.add(new JLabel("Start Date: " + data[ClientDataManager.field.ITEM_START.getValue()]));
            this.add(new JLabel("End Date: " + data[ClientDataManager.field.ITEM_END.getValue()]));
            this.add(new JLabel("Price: " + data[ClientDataManager.field.ITEM_PRICE.getValue()]));
            this.add(new JLabel("Seller: " + data[ClientDataManager.field.ITEM_SELLER.getValue()]));
            this.setVisible(true);
        } catch (NoSuchElementException e)
        {
            e.printStackTrace();
        }

    }

}
