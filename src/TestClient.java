import Comms.Communication;
import Comms.Listeners.MessageReceivedListener;

import java.io.IOException;
import java.util.Scanner;

/**
 * auction-system - PACKAGE_NAME
 * Created by matthew on 13/05/16.
 */
public class TestClient implements MessageReceivedListener
{
    public TestClient(String host, int port)
    {
        Thread clientThread;
        Communication communication;
        try
        {
            communication = new Communication(host, port);
            communication.addMessageReceivedListener(this);
            clientThread = new Thread(communication);
            clientThread.start();

            while (true)
            {
                Scanner user_input = new Scanner(System.in);
                if (communication.isClosed()) break;
                communication.sendMessage(user_input.nextLine());
            }
        }
        catch (IOException e)
        {
            System.out.println("Failed to connect to the server");
        }
    }

    public static void main(String[] args)
    {
        final String host = args.length < 1 ? "localhost" : args[0];
        final int port = args.length < 2 ? 4444 : Integer.parseInt(args[1]);
        new TestClient(host, port);
    }

    @Override
    public void messageReceived(String message)
    {
        System.out.println(message);
    }
}
