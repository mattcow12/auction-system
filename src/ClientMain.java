import Client.Client;

/**
 * auction-system - PACKAGE_NAME
 * Created by matthew on 12/05/16.
 */
class ClientMain
{
    public static void main(String[] args)
    {
        final String host = args.length < 1 ? "localhost" : args[0];
        final int port = args.length < 2 ? 4444 : Integer.parseInt(args[1]);
        new Client(host, port);
    }
}
